
#include "NodeMCUConnector.h"

NodeMCUConnector::NodeMCUConnector(){}

void NodeMCUConnector::init(int heartbeat, String firmwareVersion, String userId, int port, String host, String ssid, String password)
{  
   pinMode(LED_BUILTIN, OUTPUT);
   digitalWrite(LED_BUILTIN, HIGH);
   
   if (!Serial) {
	   Serial.begin(115200);   
   }

  _port = port;
  _host = host;
  _firmwareVersion = firmwareVersion;
  _userId = userId;

  _heartbeat = heartbeat;

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
}

NodeMCUConnector::States NodeMCUConnector::getState()
{
  return state;
}

void NodeMCUConnector::loop()
{
  // wait for WiFi connection
  if (WiFi.waitForConnectResult() != WL_CONNECTED) {
    Serial.println("Connection Failed! Rebooting...");
    delay(5000);
    ESP.restart();
  }

  if( state == DISCONNECTED ) {

    do
    {
      if (!client.connect(_host, _port)) {
          Serial.println("Socket connection failed");
        continue;
      }
        Serial.println("socket connection success");
    }  while (!client.connected());

    state = CONNECTING;

    client.setNoDelay(true);
    client.print("CONNECT " + _uuid + " " + _firmwareVersion + " " + _userId);

  } else if(state == CONNECTED) {
    currentMillisSendHeartBeat = millis();
    
    if (currentMillisSendHeartBeat - previousMillisSendHeartBeat >= _heartbeat || currentMillisSendHeartBeat < previousMillisSendHeartBeat) 
    {
      previousMillisSendHeartBeat = currentMillisSendHeartBeat;
      sendHeartBeat();
    }
  }

  // Received HeartBeat
  currentMillisReceiveHeartBeat = millis();
  if (currentMillisReceiveHeartBeat - previousMillisReceiveHeartBeat >= 2 * _heartbeat || currentMillisReceiveHeartBeat < previousMillisReceiveHeartBeat) 
  {
    previousMillisReceiveHeartBeat = currentMillisReceiveHeartBeat;
    state = DISCONNECTED;
	Serial.println("Disconnection due to inactivity");

    client.flush();
  }  
  
  if( client.available() > 0 ) 
  {
    previousMillisReceiveHeartBeat = currentMillisReceiveHeartBeat;

    delay(100);
    String response = client.readStringUntil('\n');
      
    if (response == "TUPTUP")
    {
      Serial.println("Hearbeat received");
    }
    else if( response.length() > 0 )
    {
      String frame = getValue(response, 0);

      if( frame == "CONNECTED" )
      {
        state = CONNECTED; 
        Serial.println("Connected");
      } else if (frame == "UPDATE") {
        Serial.print("Starting update from url ");
        Serial.println(getValue( response, 3));
        runUpdate(getValue( response, 1).toInt(), getValue(response, 2), getValue( response, 3), getValue( response, 4));
      } else if (frame == "MESSAGE") {
        Serial.print("Received generic message ");
        Serial.println(response.substring(8));		  
	  } else {
		Serial.print("Undefined message received ");
		Serial.println(response);		  
	  }
    }
  }  
}

void NodeMCUConnector::sendHeartBeat() {
  client.print("TUPTUP " + _uuid);
}


void NodeMCUConnector::runUpdate(int _size, String _md5, String firmwareUrl, String campaignId) {
   int _cmd = 0;

  if (!Update.begin(_size, _cmd, LED_BUILTIN, HIGH)) {
    onError("Update Begin Error", campaignId);
    return;
  }

  //_udp_ota->append("OK", 2);
  //_udp_ota->send(ota_ip, _ota_udp_port);

  HTTPClient http;
  http.begin(firmwareUrl);
  http.useHTTP10(true);
  http.setTimeout(8000);
  
  int code = http.GET();
  int len = http.getSize();
  
  if (code != 200) {
    onError("Update failed because of http code", campaignId);
	return;	
  }
  
  WiFiClient * tcp = http.getStreamPtr();
  
  delay(100);

  if (!Update.setMD5(_md5.c_str())) {
	onError("Update failed because of bad md5 given", campaignId);
	return;	  
  }

  if(Update.writeStream(*tcp) != len) {
	onError("Update write stream failed", campaignId);
	return;	  
  } 


  if (Update.end()) {
    client.print("UPDATESUCCESSFUL " + _uuid + " " + campaignId);

    delay(100);
    Serial.printf("Update Success\n");
    Serial.printf("Rebooting...\n");
      delay(100);
      ESP.restart();
  } else {
    Serial.printf("Update Error\n");
	client.print("UPDATEFAILED " + _uuid + " " + campaignId);
    Update.printError(client);
    state = DISCONNECTED;
    
  }

}


// Send Message
void NodeMCUConnector::sendMessage(String message)
{
    client.print("MESSAGE " + _uuid + " " + message);
}



String NodeMCUConnector::getValue( String content, int part)
{
  String delimeter = " ";
  int    end;
  int    start = 0;
  String result;

  // Iterate past unwanted values
  for( int count = 0; count < part; count++ )
  {
    end = content.indexOf( delimeter, start );
    start = end + delimeter.length();
  }
  
  // Get next occurance of delimeter
  // May return -1 if not found
  end = content.indexOf( delimeter, start );
  
  // If no more occurances
  if( end == -1 )
  {
    // Must be last value in content
    // Parse out remainder
    result = content.substring( start );
  } else {
    // Otherwise parse out segment of content
    result = content.substring( start, end );
  }
  
  // Clean off white space
  result.trim();
  
  // Return resulting content
  return result;
}


void NodeMCUConnector::onError(String msg, String campaignId) {
    Serial.println(msg);

    if (Update.hasError()) {
      Update.printError(Serial);
      Update.clearError();      
    }
	client.print("UPDATEFAILED " + _uuid + " " + campaignId);

    state = DISCONNECTED;	
}