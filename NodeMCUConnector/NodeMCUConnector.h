
#ifndef NodeMCUConnector_h
#define NodeMCUConnector_h
#include <ESP8266WiFi.h>
#include <Arduino.h>

#include <ESP8266WiFiMulti.h>
#include <WiFiUdp.h>

#include <ESP8266HTTPClient.h>

#include <WiFiClient.h>


class NodeMCUConnector
{
  public:
    enum States {DISCONNECTED, CONNECTING, CONNECTED, UPDATING};
    NodeMCUConnector();
    void init(int heartbeat, String firmwareVersion, String userId, int _port, String _host, String ssid, String password);
    void loop();
    States getState();
	void sendMessage(String message);
    
  private:
    States state = DISCONNECTED;

    String _uuid = String(ESP.getChipId()).c_str();
	String _userId;
	String _firmwareVersion;
    int _heartbeat;
    WiFiClient client;
	int _port;
	String _host;
    unsigned long currentMillisSendHeartBeat = millis();
    unsigned long previousMillisSendHeartBeat = millis();

    unsigned long currentMillisReceiveHeartBeat = millis();
    unsigned long previousMillisReceiveHeartBeat = millis();


    void sendHeartBeat();
    String getValue( String content, int part);
	
	void runUpdate(int _size, String _md5, String firmwarePath, String campaignId);
	void onError(String msg, String campaignId);
};

 #endif
